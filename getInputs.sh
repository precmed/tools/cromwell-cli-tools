#!/bin/bash

if [[ $# -ne 1 ]]; then
    echo "Usage: `basename $0` <run-id>"
    exit 1
fi

ID=$1

SERVER=${CROMWELL_SERVER:-http://cromwell.testbed-precmed.iit.demokritos.gr}
SERVER="${SERVER%%/}"
curl -s "$SERVER/api/workflows/v1/$1/metadata" | jq -r '.submittedFiles.inputs' | jq .
